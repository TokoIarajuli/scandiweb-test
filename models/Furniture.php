<?php 

namespace app\models;
use app\models\Product;
use app\Database;
use app\models\ProductInterface;

class Furniture extends Product implements ProductInterface {

    private $length;
    private $width;
    private $height;

    public function setLength($length) 
    {
        $this->length = $length;
    }

    public function setWidth($width) 
    {
        $this->width = $width;
    }

    public function setHeight($height) 
    {
        $this->height = $height;
    }

    public function getLength() 
    {
        return $this->length;
    }

    public function getWidth() 
    {
        return $this->width;
    }

    public function getHeight() 
    {
        return $this->height;
    }

    //Function for adding furniture into the database
    public function save()
    {
        $db = Database::$db;
        $statement = $db->pdo->prepare("INSERT INTO products(sku, name, price, height, width, length, type)
                VALUES (:sku, :name, :price, :height, :width, :length, :type)");
        $statement->bindValue(':sku', $this->getSku());
        $statement->bindValue(':name', $this->getName());
        $statement->bindValue(':price', $this->getPrice());
        $statement->bindValue(':height', $this->getHeight());     
        $statement->bindValue(':width', $this->getWidth());     
        $statement->bindValue(':length', $this->getLength());     
        $statement->bindValue(':type', $this->getType());     
        $statement->execute();
    }
    
}