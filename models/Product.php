<?php

namespace app\models;

abstract class Product 
{   
    public ?int $id = null;
    private string $sku;
    private string $name;
    private float $price;
    private string $type;

    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getSku() 
    {
        return $this->sku;
    }

    public function getName() 
    {
        return $this->name;
    }

    public function getPrice() 
    {
        return $this->price;
    }

    public function getType() 
    {
        return $this->type;
    }
    
}