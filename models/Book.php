<?php

namespace app\models;
use app\models\Product;
use app\Database;
use app\models\ProductInterface;

class Book extends Product implements ProductInterface {

    private $weight;

    public function setWeight($weight) 
    {
        $this->weight = $weight;
    }

    public function getWeight() 
    {
        return $this->weight;
    }

    //Function for adding book into the database
    public function save()
    {   
        $db = Database::$db;
        $statement = $db->pdo->prepare("INSERT INTO products(sku, name, price, weight, type)
                VALUES (:sku, :name, :price, :weight, :type)");
        $statement->bindValue(':sku', $this->getSku());
        $statement->bindValue(':name', $this->getName());
        $statement->bindValue(':price', $this->getPrice());
        $statement->bindValue(':weight', $this->getWeight());     
        $statement->bindValue(':type', $this->getType());     
        $statement->execute();
    }

}