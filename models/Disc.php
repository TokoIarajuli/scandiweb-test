<?php 

namespace app\models;
use app\models\Product;
use app\Database;
use app\models\ProductInterface;

class Disc extends Product implements ProductInterface {

    private $size;

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function getSize() 
    {
        return $this->size;
    }

    //Function for adding DVD into the database
    public function save()
    {
        $db = Database::$db;
        $statement = $db->pdo->prepare("INSERT INTO products(sku, name, price, size, type)
                VALUES (:sku, :name, :price, :size, :type)");
        $statement->bindValue(':sku', $this->getSku());
        $statement->bindValue(':name', $this->getName());
        $statement->bindValue(':price', $this->getPrice());
        $statement->bindValue(':size', $this->getSize());     
        $statement->bindValue(':type', $this->getType());     
        $statement->execute();
    }
}