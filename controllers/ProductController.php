<?php 

namespace app\controllers;
use app\Router;
use app\models\Book;
use app\models\Disc;
use app\models\Furniture;


class ProductController
{

    public function Index(Router $router)
    {   
        $products = $router->database->getProducts();
        $router->renderView('index', [
            'products' => $products
        ]);
    }

    //Get the values from create.php and call renderView() function
    //renderView will get 'create' as a $view argument in the Router.php class
    public function create(Router $router)
    {  
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $type = $_POST['p_type'];
            switch ($type) {

                case $type == 'book':
                    $book = new Book();
                    $book->setSku($_POST['sku']);
                    $book->setName($_POST['name']);
                    $book->setPrice(floatval($_POST['price']));
                    $book->setWeight(intval($_POST['weight']));  
                    $book->setType($type);    
                    $book->save();
                break;

                case $type == 'furniture':
                    $furniture = new Furniture();
                    $furniture->setSku($_POST['sku']);
                    $furniture->setName($_POST['name']);
                    $furniture->setPrice(floatval($_POST['price']));
                    $furniture->setHeight(intval($_POST['height']));
                    $furniture->setWidth(intval($_POST['width']));
                    $furniture->setLength(intval($_POST['length']));
                    $furniture->setType($type);  
                    $furniture->save();
                break;
                    
                case $type == 'disc':
                    $disc = new Disc();
                    $disc->setSku($_POST['sku']);
                    $disc->setName($_POST['name']);
                    $disc->setPrice(floatval($_POST['price']));
                    $disc->setSize(intval($_POST['size']));
                    $disc->setType($type);  
                    $disc->save();
                break;
                    
            }

            header('Location: /product/list');
            exit;  
        }
        $router->renderView('create');
    }

    //Get the array of ids from index.php, modify it and 
    //call deleteProduct() function from Database.php
    public function delete(Router $router)
    {
        $id = [];
        foreach ($_POST['delete'] as $pval) {
            $id[] = (int)$pval;
        }
        $id = implode(', ', $id);

        if (!$id) {
            header('Location: /product/list');
            exit;
        } 

        if ($router->database->deleteProduct($id)) {
            header('Location: /product/list');
            exit;
        }
    }
}