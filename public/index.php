<?php

require_once __DIR__.'/../vendor/autoload.php';

use app\controllers\ProductController;
use app\Database;
use app\Router;

$database = new Database();
$router = new Router($database);

$router->get('/', [ProductController::class, 'index']);
$router->get('/product', [ProductController::class, 'index']);
$router->get('/product/list', [ProductController::class, 'index']);
$router->get('/product/create', [ProductController::class, 'create']);
$router->post('/product/create', [ProductController::class, 'create']);
$router->post('/products/delete', [ProductController::class, 'delete']);

$router->resolve();