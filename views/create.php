<!-- form for creating products, input values are  being sent to productController.php -->
<form method="post" enctype="multipart/form-data" class="create_form">

    <div class="header">
        <h1>Add Product</h1>
        <button type="submit" class="submit_button">Submit</button>
    </div>

    <div>
        <label>SKU</label>
        <input type="text" name="sku" required>
    </div>

    <div>
        <label>Name</label>
        <input type="text" name="name" required>
    </div>

    <div>
        <label>Price</label>
        <input type="number" step=".01" name="price" required>
    </div>

    <label for="p_type">Type Switcher</label>
    <select name="p_type" id="p_type">
        <option selected disabled hidden>Type Switcher</option>
        <option value="book">Book</option>
        <option value="furniture">Furniture</option>
        <option value="disc">DVD-disc</option>
    </select>

    <div id="product_form"></div>

</form>



<script>
let type = document.getElementById("p_type");
type.addEventListener("change", add, false);

function add(e) {
    const TYPE_BOOK = 'book'
    const TYPE_FURNITURE = 'furniture'
    const TYPE_DVD = 'disc'
    const id = document.getElementById("product_form");
    option = e.target.value;

    switch (option) {
        case TYPE_BOOK:
            id.innerHTML = `
            <div class="book_input">
                <label>Weight</label>
                <input type="number" name="weight" required >
                <p>Please type the weight of the book in kilograms</p>
            </div>`;
            break;
        case TYPE_FURNITURE:
            id.innerHTML = `
            <div class="furniture_input">
                <div>
                    <label>Height</label>
                    <input type="number" name="height" required>
                </div>

                <div>
                    <label>Width</label>
                    <input type="number" name="width" required>
                </div>
                <div>
                    <label>Length</label>
                    <input type="number" name="length" required>
                </div>
                <p>Please type the dimensions of the furniture in centimetres</p>
            </div>`;
            break;
        case TYPE_DVD:
            id.innerHTML = `
            <div class="disc_input">
                <label>Size</label>
                <input type="number" name="size" required>
                <p>Please type the size of the DVD-disc in megabytes</p>
            </div>`;
            break;
    };
};
</script>