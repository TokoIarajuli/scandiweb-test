<form method="post" action="/products/delete">
    <div class="header">
        <h1>Product List</h1>
        <a href="/product/create" type="button">Add Product</a>
        <button type="submit">Mass Delete</button>
    </div>

    <!-- Render created product values. If the checkbox is checked
    respective product ids are saved to delete[] -->
    <div class="cards">
        <?php foreach ($products as $i => $product) { ?>
        <div class="card">
            <input type="checkbox" name="delete[]" value="<?php echo $product['id']; ?>">
            <p><?php echo $product['sku'] ?></p>
            <p><?php echo $product['name'] ?></p>
            <p><?php echo $product['price'] ?> $</p>
            <?php             
            switch($product['type']) {
                
                case 'book':
                    echo '<p>Weight: '.$product['weight'].' KG </p>'; 
                break;

                case 'furniture':
                    echo '<p>Dimension: '.$product['height'].'x'.$product['length'].'x'.$product['width'].'</p>';
                break;
                
                case 'disc':
                    echo '<p>Size: '.$product['size'].' MB </p>';
                break;
                
            } 
            ?>
        </div>
        <?php } ?>
    </div>
</form>