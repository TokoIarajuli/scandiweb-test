<?php 

namespace app;
use PDO;

class Database
{   

    public $pdo = null;
    public static ?Database $db = null;
    
    //Connect to the dabatase
    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=localhost;port=3306;dbname=scandiweb', 'root', '');
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$db = $this;
    }

    //Function for getting every product from the database
    public function getProducts()
    {
        $statement =  $this->pdo->prepare('SELECT * FROM products');
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    //Function for deleting products with ids from $id array 
    public function deleteProduct($id)
    {
        $statement = $this->pdo->prepare('DELETE FROM products WHERE id IN ('. $id .')');
        return $statement->execute();
    }
}